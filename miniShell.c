#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <wchar.h>
#include<windows.h>
#include<conio.h>
typedef struct _node node;
struct _node{
	char name[21];
	char type;
	node* P;
	node* FC;
	node* NS;
};

typedef struct _filesystem fs;
struct _filesystem{
	node* root;
	node* CD;
};

//Tree Functions
int comparison(node*,node*);
void display(node*);
void insert(node*,char*);
node* recinsert(node*,char*);
void clear(fs*);
void recclear(node*);

//File System Functions
fs* newFileSystem();
void destroy(fs*);
void ls(fs*,char*);
char* pwd(fs*,int);
char* recPWD(node*,char*,int,int);
void mkdir(fs*,char*);
void recAddDir(node*,char*);
void touch(fs*,char*);
void recAddFile(node*,char*);
void order(fs*);
void recOrder(node*);
void cd(fs*,char*);
void rm(fs*,char*,int);
void recremove(node*);
void rmf(fs*,char*);
void find(fs*,char*);
void recFind(fs*,node*,char*);
void findHelp(fs*,node*,char*);

fs* newFileSystem(){
	fs* f = (fs*)malloc(sizeof(fs));
	f -> root = (node*)malloc(sizeof(node));
	f -> root -> FC = NULL;
	f -> root -> NS = NULL;
	f -> root -> P = NULL;
	strcpy(f -> root -> name, "/");
	f -> CD = f -> root;
	return f;
}

int comparison(node* n1, node* n2){
	int size, x = 0;
	if(n1 -> type == 'D'){
		if(n2 -> type == 'F')
			return -1;
	}
	if(n1 -> type == 'F'){
		if(n2 -> type == 'D')
			return 1;
	}
	if(strcmp(n1 -> name, n2 -> name) == 0)
		return 0;
	if (strlen(n1 -> name) < strlen(n2 -> name))
		return -1;
	else if(strlen(n1 -> name) > strlen(n2 -> name))
		return 1;
	else
		size = strlen(n1 -> name);
	for(x = 0; x < size - 1; x++){
		char c1 = n1 -> name[x];
		char c2 = n2 -> name[x];
		if(c1 == c2)
			continue;
		if(c1 == '\0')
			return -1;
		if(c2 == '\0')
			return 1;
		if(c1 == '.')
			return -1;
		if(c2 == '.')
			return 1;
		if(c1 == '-')
			return -1;
		if(c2 == '-')
			return 1;
		if(c1 == '_')
			return -1;
		if(c2 == '_')
			return 1;
		if(c1 < c2)
			return -1;
		if(c1 > c2)
			return 1;
	}
	return 0;
}

void mkdir(fs* f, char* path){
	char* token = strtok(path, "/");
	node* temp = f -> root -> FC;
	while(1){
		if(temp == NULL){ //if file system is empty other than root
			temp = (node*)malloc(sizeof(node));
			node* newDir = (node*)malloc(sizeof(node));
			strcpy(newDir -> name, token);
			newDir -> type = 'D';
			newDir -> FC = NULL;
			newDir -> NS = NULL;
			newDir -> P = f -> root;
			f -> root -> FC = newDir;
			recAddDir(f -> root -> FC, token);
			return;
		}

		if(strcmp(temp -> name, token) == 0){
			if(temp -> FC == NULL){ //match found, but the found directory is empty
				token = strtok(NULL, "/");
				if(token == NULL){
					printf("Error Output: Make Dir Error: Cannot create directory.\n");
					return;
				}
				node* newDir = (node*)malloc(sizeof(node));
				strcpy(newDir -> name, token);
				newDir -> type = 'D';
			    newDir -> FC = NULL;
			    newDir -> NS = NULL;
			    newDir -> P = temp;
				temp -> FC = newDir;
				temp = temp -> FC;
				recAddDir(temp, token);
				return;
			}
			else{ //match found in current directory, proceed inside
				temp = temp -> FC;
				token = strtok(NULL, "/");
			}
		}
		else{ //match not found, look at the next entry in current directory
			if(temp -> NS == NULL){ //current directory exhausted, add the directory in current directory
				node* newDir = (node*)malloc(sizeof(node));
				strcpy(newDir -> name, token);
				newDir -> type = 'D';
				newDir -> FC = NULL;
				newDir -> NS = NULL;
				newDir -> P = temp -> P;
				////// ORDERING //////
				node* temp2 = temp -> P -> FC;
				int added = 0;
				newDir -> NS = temp2; //add to beginning of current directory
				temp -> P -> FC = newDir;
				node* prev = newDir -> NS;
				while(newDir -> NS != NULL){
					if(comparison(newDir, newDir -> NS) == 1){ //if it is lower precedence, then move down the list
						if(newDir == temp -> P -> FC){
							temp -> P -> FC = temp2;
						}
						node* temp3 = newDir -> NS -> NS;
						node* temp4 = newDir -> NS;
						newDir -> NS -> NS = newDir;
						newDir -> NS = temp3;
						if(prev != temp4){
							prev -> NS = temp4;
							prev = prev -> NS;
						}
						added = 1;
					}
					else{
						added = 1;
						break;
					}
				}
				////// ORDERING //////
				if(added == 0){
					temp -> NS = newDir;
					temp = temp -> NS;
				}
				recAddDir(temp, token);
				return;
			}
			else
				temp = temp -> NS;
		}
	}
}


void recAddDir(node* n, char* token){
	token = strtok(NULL, "/");
	if(token != NULL){
		node* newNode = (node*)malloc(sizeof(node));
		strcpy(newNode -> name, token);
		newNode -> type = 'D';
		newNode -> FC = NULL;
		newNode -> NS = NULL;
		newNode -> P = n;
		n -> FC = newNode;
		recAddDir(n -> FC, token);
	}
}

void touch(fs* f, char* path){
	char* token = strtok(path, "/");
	node* temp = f -> root -> FC;
	while(1){
		if(temp == NULL){ //if file system is empty other than root
			temp = (node*)malloc(sizeof(node));
			node* newFile = (node*)malloc(sizeof(node));
			strcpy(newFile -> name, token);
			newFile -> type = 'D';
			newFile -> FC = NULL;
			newFile -> NS = NULL;
			newFile -> P = f -> root;
			f -> root -> FC = newFile;
			token = strtok(NULL, "/"); ///
			recAddFile(f -> root -> FC, token);
			return;
		}
		if(strcmp(temp -> name, token) == 0){
			if(temp -> FC == NULL){ //match found, but the found directory is empty
				token = strtok(NULL, "/");
				if(token == NULL){
					printf("Error Output: Touch Error: Cannot create file.\n");
					return;
				}
				node* newFile = (node*)malloc(sizeof(node));
				strcpy(newFile -> name, token);
				newFile -> type = 'D';
			    newFile -> FC = NULL;
			    newFile -> NS = NULL;
			    newFile -> P = temp;
				temp -> FC = newFile;
				temp = temp -> FC;
				token = strtok(NULL, "/"); ///
				recAddFile(temp, token);
				return;
			}
			else{ //match found in current directory, proceed inside
				temp = temp -> FC;
				token = strtok(NULL, "/");
			}
		}
		else{ //match not found, look at the next entry in current directory
			if(temp -> NS == NULL){ //current directory exhausted, add the directory in current directory
				node* newFile = (node*)malloc(sizeof(node));
				strcpy(newFile -> name, token);
				token = strtok(NULL, "/");
				if(token == NULL)
					newFile -> type = 'F';
				else
					newFile -> type = 'D';
				newFile -> FC = NULL;
				newFile -> NS = NULL;
				newFile -> P = temp -> P;
				////// ORDERING //////
				node* temp2 = temp -> P -> FC;
				int added = 0;
				newFile -> NS = temp2; //add to beginning of current directory
				temp -> P -> FC = newFile;
				node* prev = newFile -> NS;
				while(newFile -> NS != NULL){
					if(comparison(newFile, newFile -> NS) == 1){ //if it is lower precedence, then move down the list
						if(newFile == temp -> P -> FC){
							temp -> P -> FC = temp2;
						}
						node* temp3 = newFile -> NS -> NS;
						node* temp4 = newFile -> NS;
						newFile -> NS -> NS = newFile;
						newFile -> NS = temp3;
						if(prev != temp4){
							prev -> NS = temp4;
							prev = prev -> NS;
						}
						added = 1;
					}
					else{
						added = 1;
						break;
					}
				}
				////// ORDERING //////
				if(added == 0){
					temp -> NS = newFile;
					temp = temp -> NS;
				}
				recAddFile(newFile, token);
				return;
			}
			else
				temp = temp -> NS;
		}
	}
}

void recAddFile(node* n, char* token){
	if(token == NULL){
		n -> type = 'F';
	}
	if(token != NULL){
		node* newNode = (node*)malloc(sizeof(node));
		strcpy(newNode -> name, token);
		newNode -> type = 'D';
		newNode -> FC = NULL;
		newNode -> NS = NULL;
		newNode -> P = n;
		n -> FC = newNode;
		token = strtok(NULL, "/");
		recAddFile(n -> FC, token);
	}
}

void order(fs* f){
	recOrder(f -> root -> FC);
}

void recOrder(node* n){
	if(n -> NS != NULL){
		printf("%s", n -> name);
		if(comparison(n, n -> NS) == 1){
			node* temp = n;
			n = n -> NS;
			n -> NS = temp;
			recOrder(n);
		}
	}
	if(n -> NS != NULL){
		recOrder(n -> NS);
	}
}

void recRemove(node* n){
	if(n -> FC != NULL){
		recRemove(n -> FC);
	}
	if(n -> NS != NULL){
		recRemove(n -> NS);
	}
	node* parent = n -> P;
	parent -> FC = NULL;
	free(n);

}

void rm(fs* f, char* path, int force){
	char* token = strtok(path, "/");
	node* temp = f -> root -> FC;
	while(1){
		if(temp == NULL){
			printf("Error Output: Remove Error: Cannot remove file or directory.\n");
			return;
		}
		if(strcmp(temp -> name, token) == 0){
			if(temp -> FC == NULL){
				token = strtok(NULL, "/");
				if(token == NULL){
					node* parent = temp -> P;
					parent -> FC = NULL;
					free(temp);
					return;
				}
				else{
					printf("Error Output: Remove Error: Cannot remove file or directory.\n");
					return;
				}
			}
			else{
				token = strtok(NULL, "/");
				if(token == NULL){
					if(force == 1){
						recRemove(temp);
						return;
					}
					else{
						printf("Error Output: Remove Error: directory '%s' is not empty.\n", path);
						return;
					}
				}
				temp = temp -> FC;
			}
		}
		else{
			if(temp -> NS == NULL){
				printf("Error Output: Remove Error: Cannot remove file or directory.\n");
				return;
			}
			else{
				temp = temp -> NS;
			}
		}
	}
}

void find(fs* f, char* name){
	printf("Searching For '%s':\n", name);
	recFind(f,f -> root -> FC, name);
}

void recFind(fs* f, node* n, char* name){
	findHelp(f, n, name);
	if(n -> FC != NULL){
		recFind(f, n -> FC, name);
	}
	if(n -> NS != NULL){
		recFind(f, n -> NS, name);
	}
}

void findHelp(fs* f, node* n, char* name){
	int i = 0;
	int j = 0;
	int c = 0;
	char PWD[1000];
	memset(PWD, 0, 1000);
	int complete = strlen(name);
	for(i = 0; i < strlen(n -> name); i++){
		if(n -> name[i] == name[j]){
			c++;
			j++;
			if(c == complete){
				node* temp = f -> CD;
				f -> CD = n;
				strcpy(PWD, pwd(f,1));
				printf("%c ", n -> type);
				printf("%s\n", PWD);
				f -> CD = temp;
				break;
			}
		}
	}
}

char* pwd(fs* f,int finder){
	char PWD[1000];
	memset(PWD, 0, 1000);
	if(f -> CD == f -> root){
		strcpy(PWD, "/");
		return PWD;
	}
	return recPWD(f -> CD, PWD, 0, finder);
}

char* recPWD(node* n, char* PWD, int first, int finder){
	char* temp;
	char temp2[21];
	if(n != NULL){
		temp = strdup(PWD);
		if(strcmp(n -> name, "/") != 0 && first > 0){
			strcpy(temp2, n -> name);
			strcat(temp2, "/");
			strcpy(PWD, temp2);
		}
		else{
			printf('\0');
			strcpy(PWD, n -> name);
		}
		strcat(PWD, temp);
		recPWD(n -> P, PWD, 1, finder);
	}
	free(temp);
	return PWD;
}

void cd(fs* f, char* path){
	if(strcmp(path, "/") == 0){
		f -> CD = f -> root;
		return;
	}
	char* token = strtok(path, "/");
	node* temp = f -> root -> FC;

	while(1){
		if(token == NULL || (strcmp(token, ".") != 0 && strcmp(token, "..") != 0)){ //return to normal cd method
			break;
		}
		if(strcmp(token, ".") == 0){
			temp = f -> CD;
			token = strtok(NULL, "/");
			if(token == NULL){
				f -> CD = temp;
				return;
			}
		}
		else if(strcmp(token, "..") == 0){
			if(f -> CD == f -> root)
				temp = f -> root;
			else{
				temp = f -> CD -> P;
				f -> CD = f -> CD -> P;
			}
			token = strtok(NULL, "/");
			if(token == NULL){
				f -> CD = temp;
				return;
			}
		}
	}

	while(1){
		if(temp == NULL){
			printf("Error Output: Change Dir Error: Cannot change working directory.\n");
			return;
		}
		if(strcmp(temp -> name, token) == 0){
			if(temp -> FC == NULL){
				token = strtok(NULL, "/");
				if(token == NULL){
					if(temp -> type == 'D'){
						f -> CD = temp;
						return;
					}
					else{
						printf("Error Output: Change Dir Error: Cannot change working directory.\n");
						return;
					}
				}
				else{
					printf("Error Output: Change Dir Error: Cannot change working directory.\n");
					return;
				}
			}
			else{
				token = strtok(NULL, "/");
				if(token == NULL){
					if(temp -> type == 'D'){
						f -> CD = temp;
						return;
					}
					else{
						printf("Error Output: Change Dir Error: Cannot change working directory.\n");
						return;
					}
				}
				temp = temp -> FC;
			}
		}
		else{
			if(temp -> NS == NULL){
				printf("Error Output: Change Dir Error: Cannot change working directory.\n");
				return;
			}
			else{
				temp = temp -> NS;
			}
		}
	}
}

void ls(fs* f, char* path){
	if(strcmp(path, "") == 0){
		node* temp = f -> CD -> FC;
		char* PWD = pwd(f,0);
		printf("Listing For '%s':\n", PWD);
		while(temp != NULL){
			if(temp -> type == 'D')
				printf("D ");
			    printf("%s\n", temp -> name);
			    temp = temp -> NS;
			else
				printf("F ");
			printf("%s\n", temp -> name);
			temp = temp -> NS;
		}
	}
	else{
		char* token = strtok(path, "/");
		node* temp = f -> root -> FC;
		while(1){
			if(temp == NULL){
				printf("Error Output: List Error: Cannot perform list operation.\n");
				return;
			}
			if(strcmp(token, ".") == 0){
				temp = f -> CD;
				token = strtok(NULL, "/");
				if(token == NULL)
					break;
			}
			if(strcmp(token, "..") == 0){
				if(f -> CD == f -> root){
					temp = f -> root;
				}
				else{
					temp = f -> CD -> P;
					f -> CD = f -> CD -> P;
				}
				token = strtok(NULL, "/");
				if(token == NULL)
					break;
			}
			if(strcmp(temp -> name, token) == 0){
				if(temp -> FC == NULL){ //match found, but the found directory is empty
					token = strtok(NULL, "/");
					if(token == NULL)
						break;
					else{
						printf("Error Output: List Error: Cannot perform list operation.\n");
						return;
					}
				}
				else{ //match found in current directory, proceed inside
					token = strtok(NULL, "/");
					if(token == NULL){
						break;
					}
					temp = temp -> FC;
				}
			}
			else{ //match not found, look at the next entry in current directory
				if(temp -> NS == NULL){ //current directory exhausted, add the directory in current directory
					printf("Error Output: List Error: Cannot perform list operation.\n");
					return;
				}
				else
					temp = temp -> NS;
			}
		}
		if(temp -> type == 'F'){
			printf("Error Output: List Error: Cannot perform list operation. '%s' is a file.\n", path);
			return;
		}
		else{
			printf("Listing For '/%s':\n", path);
			temp = temp -> FC;
			while(temp != NULL){
				if(temp -> type == 'D')
					printf("D ");
				printf("%s\n", temp -> name);
				temp = temp -> NS;
				else
					printf("F ");
				printf("%s\n", temp -> name);
				temp = temp -> NS;
			}
		}
	}
}

void lsd(fs* f, char* path){
	if(strcmp(path, "") == 0){
		node* temp = f -> CD -> FC;
		char* PWD = pwd(f,0);
		printf("Listing For '%s':\n", PWD);
		while(temp != NULL){
			if(temp -> type == 'D'){
				printf("D ");
				printf("%s\n", temp -> name);
                }
                temp = temp -> NS;

		}
	}

}

void lsf(fs* f, char* path){
	if(strcmp(path, "") == 0){
		node* temp = f -> CD -> FC;
		char* PWD = pwd(f,0);
		printf("Listing For '%s':\n", PWD);
		while(temp != NULL){
			if(temp -> type == 'F'){
				printf("%s\n", temp -> name);
                }
                temp = temp -> NS;

		}
	}

}

void HELP(char* str){
    if(strcmp(str, "touch") == 0)
    {

        printf("\t=============================================================================================================\n");
        printf("\t| touch <nom>:  cree un fichier vide, sans aucune donnee associee.                                        |\n");
        printf("\t=============================================================================================================\n");
    }
    if(strcmp(str, "mkdir") == 0){
        printf("\t=============================================================================================================\n");
        printf("\t| mkdir <nom>:   cree un repertoire vide, sans aucun fils.                                              |\n");
        printf("\t=============================================================================================================\n");
         printf("\t=============================================================================================================\n");
        printf("\t|NOTE:cette commande  affichera une erreur si la ressource specifiee n'existe pas.                          |\n");
        printf("\t=============================================================================================================\n");
    }
    if(strcmp(str, "find") == 0){
        printf("\t=============================================================================================================\n");
        printf("\t| find <nom>:    recherche toutes les ressources portant le nom specifie dans le systeme de fichiers. |\n");
        printf("\t=============================================================================================================\n");
        printf("\t=============================================================================================================\n");
        printf("\t|NOTE: <find> elle donne Le chemin de toutes les ressources rencontrees .                              \n");
        printf("\t=============================================================================================================\n");
    }
    if(strcmp(str, "pwd") == 0){
        printf("\t=============================================================================================================\n");
        printf("\t| pwd:    affiche le chemin vers le repertoire courant.                                                  |\n");
        printf("\t=============================================================================================================\n");
    }
    if(strcmp(str, "ls") == 0){
        printf("\t=============================================================================================================\n");
        printf("\t| ls:    liste tous les elements du repertoire courant.avec d'autres parametres  -D/-F/..               |\n");
        printf("\t=============================================================================================================\n");
    }
    if(strcmp(str,"--")==0)
        {
printf("La structure du systeme de fichiers peut distinguer 11 commandes differentes:\n");
printf("\n");
printf("\t=============================================================================================================\n");
printf("\t| touch <nom>:  cree un fichier vide, sans aucune donnee associee.                                        |\n");
printf("\t=============================================================================================================\n");
printf("\t| mkdir <nom>:   cree un repertoire vide, sans aucun fils.                                              |\n");
printf("\t=============================================================================================================\n");
printf("\t| cd <nom>:   prend le chemin et passe d'un repertoire a un autre.                                          |\n");
printf("\t=============================================================================================================\n");
printf("\t| rm <nome>:supprime un fichier ou bien repertoire vide !!                                           |\n");
printf("\t=============================================================================================================\n");
printf("\t| rm -f <nom>:    supprime recursivement un repertoire et tous ses descendants.                      |\n");
printf("\t=============================================================================================================\n");
printf("\t|NOTE:cette commande  affichera une erreur si la ressource specifiee n'existe pas.                          |\n");
printf("\t=============================================================================================================\n");
printf("\t| find <nom>:    recherche toutes les element portant le nom specifie dans le systeme de fichiers.    |\n");
printf("\t=============================================================================================================\n");
printf("\t| ls:    liste tous les elements du repertoire courant.Avec d'autre parametre d/f/..                  |\n");
printf("\t=============================================================================================================\n");
printf("\t| pwd:    affiche le chemin absolu vers le repertoire courant.                                           |\n");
printf("\t=============================================================================================================\n");
printf("\t| help : si vous avez un probleme tapez help  !!                                                        |\n");
printf("\t=============================================================================================================\n");
printf("\t|NOTE: Si vous tapez help <nome du commande> vous aurez les information necessaire !!                       |\n");
printf("\t=============================================================================================================\n");
printf("\t| renommer <oldname><newname>:     pour renommer un fichier et le donner le newname                         |\n");
printf("\t=============================================================================================================\n");
printf("\t| quitter:    termine l'execution du programme de gestion du systeme de fichiers.                           |\n");
printf("\t=============================================================================================================\n");
                    }
}

void renommer(fs* f, char* path){
    char* token = strtok(path, "/");
	node* temp = f -> root -> FC;
	while(1){
		if(temp == NULL){ //si le systeme de fichiers est vide autre que root
			temp = (node*)malloc(sizeof(node));
			node* newFile = (node*)malloc(sizeof(node));  //pour creer le systeme de fichier
			strcpy(newFile -> name, token);
			newFile -> type = 'D';
			newFile -> FC = NULL;
			newFile -> NS = NULL;
			newFile -> P = f -> root;
			f -> root -> FC = newFile;
			token = strtok(NULL, "/"); ///
			recAddFile(f -> root -> FC, token);  //creation des fichier recursivement

			return;
		}
		if(strcmp(temp -> name, token) == 0){
			if(temp -> FC == NULL){ //correspondance trouvee, mais le repertoire trouve est vide
				token = strtok(NULL, "/");
				if(token == NULL){
                    printf("\033[1;31m");
					printf("impossible de renommer le fichier.\n");
                    printf("\033[0m");
					return;
				}
				node* newFile = (node*)malloc(sizeof(node));
				strcpy(newFile -> name, token);
				newFile -> type = 'D';
			    newFile -> FC = NULL;
			    newFile -> NS = NULL;
			    newFile -> P = temp;
				temp -> FC = newFile;
				temp = temp -> FC;
				token = strtok(NULL, "/"); ///
				recAddFile(temp, token);
				 printf("\033[1;32m");
					printf("fichier renommer avec succes.\n");
                    printf("\033[0m");
				return;
			}
			else{ //correspondance trouvee dans le repertoire courant, continuez vers l'interieur
				temp = temp -> FC;
				token = strtok(NULL, "/");
			}
		}
		else{ //correspondance introuvable, regardez l'entree suivante dans le repertoire courant
			if(temp -> NS == NULL){ //repertoire courant epuise, ajoutez le repertoire dans le repertoire courant
				node* newFile = (node*)malloc(sizeof(node));
				strcpy(newFile -> name, token);
				token = strtok(NULL, "/");
				if(token == NULL)
					newFile -> type = 'F';
				else
					newFile -> type = 'D';
				newFile -> FC = NULL;
				newFile -> NS = NULL;
				newFile -> P = temp -> P;
				printf("\033[1;32m");
					printf("fichier renommer avec succes.\n");
                    printf("\033[0m");
				////// ORDERING //////
				node* temp2 = temp -> P -> FC;
				int added = 0;
				newFile -> NS = temp2; // ajouter au debut du repertoire courant
				temp -> P -> FC = newFile;
				node* prev = newFile -> NS;
				while(newFile -> NS != NULL){
					if(comparison(newFile, newFile -> NS) == 1){ //
//si la priorite est inferieure, descendez dans la liste
						if(newFile == temp -> P -> FC){
							temp -> P -> FC = temp2;
						}
						node* temp3 = newFile -> NS -> NS;
						node* temp4 = newFile -> NS;
						newFile -> NS -> NS = newFile;
						newFile -> NS = temp3;
						if(prev != temp4){
							prev -> NS = temp4;
							prev = prev -> NS;
						}

						added = 1;
					}
					else{

						added = 1;
						break;
					}
				}
				////// ORDERING //////
				if(added == 0){
					temp -> NS = newFile;
					temp = temp -> NS;
				}
				recAddFile(newFile, token);
				return;
			}
			else
				temp = temp -> NS;
		}
	}
}
void renommerrec(fs* f, char* path, int force){
	char* token = strtok(path, "/");
	node* temp = f -> root -> FC;
	while(1){
		if(temp == NULL){
			return;
		}
		node* temp2 = f -> root -> FC;
		node* temp3 = f -> root -> FC;
		if(strcmp(temp -> name, token) == 0){
			if(temp -> FC == NULL){
				token = strtok(NULL, "/");
				if(token == NULL){
					node* parent = temp -> P;
					temp2=temp->NS;
					parent -> FC = NULL;
					parent->FC=temp2;
					free(temp);

					return;
				}
				else{
					return;
				}
			}
			else{
				token = strtok(NULL, "/");
				if(token == NULL){
					if(force == 1){
						recRemove(temp);
						return;
					}
					else{
						return;
					}
				}
				temp = temp -> FC;
			}
		}
		else{
			if(temp -> NS == NULL){

				return;
			}
			else{
				temp = temp -> NS;
			}
		}
	}
}



int main(){
    char choix;
    int n=0;
    //la gestion de l'interface

do{     system ("cls");
        system ("Color 79 ");

        printf("\t\t\t\t\t@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
        printf("\t\t\t\t\t@@         ");
        printf("\033[1;31m");
        printf("miniShell");
        printf("\033[1;34m");
        printf("            @@\n");
        printf("\t\t\t\t\t||                                    ||\n");

        printf("\t\t\t\t\t||                                    ||\n");
        printf("\t\t\t\t\t@@   Realise par :                    @@\n");
        printf("\t\t\t\t\t||     ");
        printf("\033[1;30m");
        printf("* KASSEL MOHAMMED ISSAM   ");
        printf("\033[1;34m");
        printf("     ||\n");        printf("\t\t\t\t\t||     ");
        printf("\033[1;34m");
        printf("     @@\n");
        printf("\t\t\t\t\t||                                    ||\n");
        printf("\t\t\t\t\t@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
        printf("\033[1;31m");
        printf("\t\t\t\t\t        System de gestion de fichier    \n");
        printf("\033[1;34m");
        printf("\t\t\t\t\t________________________________________\n");
        printf("\t\t\t\t\t| |                                    |\n");
        printf("\t\t\t\t\t|1|      Ouvrir le gestionnaire        |\n");
        printf("\t\t\t\t\t| |                                    |\n");
        printf("\t\t\t\t\t========================================\n");
        printf("\t\t\t\t\t| |                                    |\n");
        printf("\t\t\t\t\t|2|     Les indications d'utilisation  |\n");
        printf("\t\t\t\t\t| |                                    |\n");
        printf("\t\t\t\t\t========================================\n");
        printf("\t\t\t\t\t| |                                    |\n");
        printf("\t\t\t\t\t|3|      Quitter l'application         |\n");
        printf("\t\t\t\t\t|_|____________________________________|\n");
        printf("\n\r Tapez svp votre choix ?  (1/2/3) \n");
        choix=toupper(getch());
        switch(choix){
			case '1' :system ("cls");
                     system ("color 7");
                        printf("\t\t\t\t\t________________________________________\n");
                        printf("\t\t\t\t\t||                                    ||\n");
                        printf("\t\t\t\t\t||     System de gestion de fichier   ||\n");
                        printf("\t\t\t\t\t||                                    ||\n");
                        printf("\t\t\t\t\t========================================\n");

    fs* f = newFileSystem();
    printf("\033[5;33m");
    printf("\t\t\t\t\tBienvenu dans le systeme de fichiers :) !! \n" );
    printf("\033[0m");

	char command[20];
	char path[1000];
	char nom[1000];
	char name[40];
	char newname[40];
	while(scanf("%s", command) > 0){
                if((strcmp(command, "renommer") != 0) && (strcmp(command, "mkdir") != 0) && (strcmp(command, "touch") != 0) && (strcmp(command, "rm") != 0) && (strcmp(command, "rm -f") != 0) && (strcmp(command, "cd") != 0) && (strcmp(command, "pwd") != 0) && (strcmp(command, "ls") != 0 )&& (strcmp(command, "lsd") != 0 )&& (strcmp(command, "lsf") != 0 )&& (strcmp(command, "find") != 0) && (strcmp(command, "quitter") != 0) && (strcmp(command, "help") != 0 )){
			printf("\033[1;31m");
			printf("La Command %s est introuvable :( pour plus d'information tapez help -- !!\n",command);
            printf("\033[0m");
		}
		if(strcmp(command, "mkdir") == 0){
        if(n==0){scanf("%s",path );mkdir(f, path);cd(f, path);} //la creation de l'arbre pour la 1 er fois
                else{
			scanf("%s",nom );
			char* PWD = pwd(f,0);
			int i;
			for (i = 0; PWD[i]!='\0'; i++);
			PWD[i]='/';
			i++;

            for (int j = 0; nom[j]!='\0'; j++, i++)
                {
                    PWD[i] = nom[j];
                }
                PWD[i] = '\0';
			mkdir(f, PWD);
                }
		}
		 

		if(strcmp(command, "touch") == 0){
            scanf("%s",nom );
			char* PWD = pwd(f,0);
			int i;
			for (i = 0; PWD[i]!='\0'; i++);
			PWD[i]='/';
			i++;

            for (int j = 0; nom[j]!='\0'; j++, i++)
                {
                    PWD[i] = nom[j];
                }
                PWD[i] = '\0';
			touch(f, PWD);
		}

		if(strcmp(command, "rm") == 0){
			scanf("%s", nom);
			if(strcmp(nom, "-f") == 0){
            scanf("%s", nom);
			char* PWD = pwd(f,0);
			int i;
			for (i = 0; PWD[i]!='\0'; i++);
			PWD[i]='/';
			i++;

            for (int j = 0; nom[j]!='\0'; j++, i++)
                {
                    PWD[i] = nom[j];
                }
                PWD[i] = '\0';
				rm(f, PWD, 1);
			}
			else{
			char* PWD = pwd(f,0);
			int i;
			for (i = 0; PWD[i]!='\0'; i++);
			PWD[i]='/';
			i++;

            for (int j = 0; nom[j]!='\0'; j++, i++)
                {
                    PWD[i] = nom[j];
                }
                PWD[i] = '\0';
				rm(f, PWD, 0);
			}
		}
		if(strcmp(command, "rm -f") == 0){
		    scanf("%s", nom);
			char* PWD = pwd(f,0);
			int i;
			for (i = 0; PWD[i]!='\0'; i++);
			PWD[i]='/';
			i++;

            for (int j = 0; nom[j]!='\0'; j++, i++)
                {
                    PWD[i] = nom[j];
                }
                PWD[i] = '\0';
				rm(f, PWD, 1);
		}
		if(strcmp(command, "find") == 0){
			scanf("%s", name);
			find(f, name);
		}
		if(strcmp(command, "cd") == 0){
			scanf("%s", nom);
                if(strcmp(nom, "..") != 0)
                {
                char* PWD = pwd(f,0);
                int i;
                for (i = 0; PWD[i]!='\0'; i++);
                PWD[i]='/';
                i++;

                for (int j = 0; nom[j]!='\0'; j++, i++)
                {
                    PWD[i] = nom[j];
                }
                PWD[i] = '\0';
			   cd(f, PWD);
                }
                else
                    cd(f,nom);
		}

		if(strcmp(command, "quitter") == 0){
			system ("cls");
                        system ("Color 79");
			           printf("\t\t\t\t\t________________________________________\n");
                       printf("\t\t\t\t\t||                                    ||\n");
                       printf("\t\t\t\t\t||    Fin du Programme Gestionnair    ||\n");
                       printf("\t\t\t\t\t||                                    ||\n");
                       printf("\t\t\t\t\t========================================\n");
                exit(0);
		}
		if(strcmp(command, "pwd") == 0){
			char* PWD = pwd(f,0);
			printf("%s\n", PWD);
		}
		if(strcmp(command, "lsd") == 0){
			memset(path, 0, 1000);
			gets(path);
			if(path[0] == '0'){
				strcpy(path, "");
			}
			else{
				char* p = path;
				p += 1;
				strcpy(path, p);
			}
			lsd(f, path);
		}
		else
            if(strcmp(command, "lsf") == 0){
			memset(path, 0, 1000);
			gets(path);
			if(path[0] == '0'){
				strcpy(path, "");
			}
			else{
				char* p = path;
				p += 1;
				strcpy(path, p);
			}
			lsf(f, path);
            }
        if(strcmp(command, "ls") == 0){
			memset(path, 0, 1000);
			gets(path);
			if(path[0] == '0'){
				strcpy(path, "");
			}
			else{
				char* p = path;
				p += 1;
				strcpy(path, p);
			}
			ls(f, path);
            }
        if(strcmp(command, "renommer") ==0){


            scanf("%s", nom);
            scanf("%s",newname );
			char* PWD = pwd(f,0);
			int i;
			for (i = 0; PWD[i]!='\0'; i++);
			PWD[i]='/';
			i++;

            for (int j = 0; nom[j]!='\0'; j++, i++)
                {
                    PWD[i] = nom[j];
                }
                PWD[i] = '\0';
				renommerrec(f, PWD, 0);


			char* PWDy = pwd(f,0);
			int k;
			for (k = 0; PWDy[k]!='\0'; k++);
			PWD[k]='/';
			k++;

            for (int j = 0; newname[j]!='\0'; j++, k++)
                {
                    PWDy[k] = newname[j];
                }
                PWDy[k] = '\0';
			renommer(f, PWDy);

                }
        if (strcmp(command, "help") == 0){
                        scanf("%s", path);
                        HELP(path);
                }
        if(n=1)
            {
                char* PWD = pwd(f,0);
			printf("%s  >>\t", PWD);
            }
            n=1;

	}
	case '2' : system ("cls");
                    system ("Color 79");
                       printf("\t\t\t\t\t________________________________________\n");
                       printf("\t\t\t\t\t||                                    ||\n");
                       printf("\t\t\t\t\t||    les indications d'utilisation   ||\n");
                       printf("\t\t\t\t\t||                                    ||\n");
                       printf("\t\t\t\t\t========================================\n");
                       printf("\n");
printf("La structure du systeme de fichiers peut distinguer les commandes suivantes:\n");
printf("\n");
printf("\t=============================================================================================================\n");
printf("\t| touch <nom>:  cree un fichier vide, sans aucune donnee associee.                                        |\n");
printf("\t=============================================================================================================\n");
printf("\t| mkdir <nom>:   cree un repertoire vide, sans aucun fils.                                              |\n");
printf("\t=============================================================================================================\n");
printf("\t| cd <nom>:   prend le chemin et passe d'un repertoire a un autre.                                          |\n");
printf("\t=============================================================================================================\n");
printf("\t| rm <nome>:supprime un fichier ou bien repertoire vide !!                                           |\n");
printf("\t=============================================================================================================\n");
printf("\t| rm -f <nom>:    supprime recursivement un repertoire et tous ses descendants.                      |\n");
printf("\t=============================================================================================================\n");
printf("\t|NOTE:cette commande  affichera une erreur si la ressource specifiee n'existe pas.                          |\n");
printf("\t=============================================================================================================\n");
printf("\t| find <nom>:    recherche toutes les element portant le nom specifie dans le systeme de fichiers.    |\n");
printf("\t=============================================================================================================\n");
printf("\t| ls:    liste tous les elements du repertoire courant.Avec d'autre parametre -D/-F/..                  |\n");
printf("\t=============================================================================================================\n");
printf("\t| pwd:    affiche le chemin absolu vers le repertoire courant.                                           |\n");
printf("\t=============================================================================================================\n");
printf("\t| help : si vous avez un probleme tapez help   !!                                                          |\n");
printf("\t=============================================================================================================\n");
printf("\t|NOTE: Si vous tapez help <nome du commande> vous aurez les information necessaire !!                       |\n");
printf("\t=============================================================================================================\n");
printf("\t| quitter:    termine l'execution du programme de gestion du systeme de fichiers.                           |\n");
printf("\t=============================================================================================================\n");
printf("\t| renommer <oldname><newname>:     pour renommer un fichier et le donner le newname                         |\n");
printf("\t=============================================================================================================\n");


			           printf("\nAppuyer sur une touche pour revenir au menu\n");
                       getch();
			           break;
            case '3' : system ("cls");
                        system ("Color 79");
			           printf("\t\t\t\t\t________________________________________\n");
                       printf("\t\t\t\t\t||                                    ||\n");
                       printf("\t\t\t\t\t||       Quitter l'application        ||\n");
                       printf("\t\t\t\t\t||                                    ||\n");
                       printf("\t\t\t\t\t========================================\n");
                exit(0);
                 break;

            default:
                    {
                        printf("numero de menu invalide\n");
                        Sleep(500);
                    }
        }
}while(1==1);

}
